def t9_encode(in_name, out_name):
    to_t9 = {'a': '2', 'b': '22', 'c': '222', 'd': '3', 'e': '33', 'f': '333',
             'g': '4', 'h': '44', 'i': '444', 'j': '5', 'k': '55', 'l': '555',
             'm': '6', 'n': '66', 'o': '666', 'p': '7', 'q': '77', 'r': '777',
             's': '7777', 't': '8', 'u': '88', 'v': '888', 'w': '9', 'x': '99',
             'y': '999', 'z': '9999', ' ': '0'}

    with open(in_name, 'r') as tests, open(out_name, 'w') as out:
        case = 'Case #{}: {}\n'.format
        for case_num, test in enumerate(tests):
            if case_num == 0:                   # skip first line
                continue
            num_str = ''                        # t9 encoded string of numbers
            prev = None                         # previous number
            for char in test.rstrip('\n'):
                curr = to_t9[char]              # current t9 value
                zero = curr[0]                  # 0th value of curr
                num_str = ''.join((num_str, ' ' if zero == prev else '', curr))
                prev = zero                     # keep track of previous value
            out.write(case(case_num, num_str))  # write to output file

t9_encode('C-small-practice.in', 'C-small_output.txt')
t9_encode('C-large-practice.in', 'C-large_output.txt')
