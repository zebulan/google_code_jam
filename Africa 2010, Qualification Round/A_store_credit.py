def store_credit(in_name, out_name):
    with open(in_name, 'r') as tests, open(out_name, 'w') as out:
        case = 'Case #{}: {}\n'.format
        case_num = 1
        credit = 0
        line = None
        for test in tests:
            if line is None:  # first line
                line = 1
                continue
            current = test.rstrip()
            if line == 1:     # credit
                credit = int(current)
            elif line == 3:   # items
                items = [int(a) for a in current.split()]
                answer = None
                for b, item in enumerate(items):
                    for c in range(b+1, len(items)):
                        if item + items[c] == credit:
                            answer = (str(b+1), str(c+1))
                            break
                    if answer is not None:
                        break
                out.write(case(case_num, ' '.join(answer)))
                case_num += 1

            line = 1 if line == 3 else line + 1

store_credit('A-small-practice.in', 'A-small_output.txt')
store_credit('A-large-practice.in', 'A-large_output.txt')
