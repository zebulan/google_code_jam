def reverse_words(in_name, out_name):
    with open(in_name, 'r') as tests, open(out_name, 'w') as out:
        case = 'Case #{}: {}\n'.format
        for index, test in enumerate(tests):
            if index == 0:
                continue
            out.write(case(index, ' '.join(reversed(test.rstrip().split()))))

reverse_words('B-small-practice.in', 'B-small_output.txt')
reverse_words('B-large-practice.in', 'B-large_output.txt')
