def numbers(in_name, out_name):
    with open(in_name, 'r') as tests, open(out_name, 'w') as out:
        case = 'Case #{}: {}\n'.format
        for dex, t in enumerate(tests):
            if dex == 0:  # skip first input which it T (# of test cases)
                continue
            base = 5.23606797749979  # (3 + (5**0.5))
            out.write(case(dex, str(int(base**int(t)))[-3:].zfill(3)))
            # above must be fixed. only works for small inputs


# numbers('C-small-practice.in', 'C-small_output.txt')
# # numbers('C-large-practice.in', 'C-large_output.txt')

nums = list()
cnt = 0
while True:
    current = str(7 ** cnt)
    last_3 = current.zfill(3) if len(current) < 3 else current[-3:].zfill(3)
    # print(last_3)
    if last_3 in nums:
        break
    nums.append(last_3)
    cnt += 1

# ['001', '007', '049', '343', '401', '807', '649',
#  '543', '801', '607', '249', '743', '201', '407',
#  '849', '943', '601', '207', '449', '143']

print(nums)
print(cnt)

# 7 ** 21 == 558545864083284007
# 7 ** 1 == 007

# modulo is the key for this one.
# adjust above to strip numbers after the decimal, int() or else find('.')??
# int() may be better in case there is no decimal, whole number result.
# maybe adjust, for fun, the length of the digits before the decimal
